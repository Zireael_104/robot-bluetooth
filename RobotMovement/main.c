#include <msp430.h>


/**
 * main.c
 */

volatile unsigned int cptTimer=0; // Compteur
unsigned int memoTimer =0; // Dur闁?d'une 闁燼pe

unsigned char state=1;
unsigned char car=0;

#define CMDLEN  12               // longueur maximum de la commande utilisateur
#define LF      0x0A
#define CR      0x0D
#define SCK         BIT5            // Serial Clock
#define DATA_OUT    BIT6            // DATA out
#define DATA_IN     BIT7            // DATA in

void Init_Robot(){ // D闁檃rrage du robot
    P2DIR|=BIT1; // Sens de la roue droite
    P2OUT|=BIT1;

    P2DIR|=BIT2; // Fonctionnement de la roue gauche
    P2OUT&=~BIT2;

    P2DIR|=BIT5; // Sens de la roue gauche
    P2OUT|=BIT5;

    P2DIR|=BIT4; // Fonctionnement de la roue droite
    P2OUT&=~BIT4;


    __enable_interrupt();
}

void Reculer_Robot(){
    P2OUT|=(BIT1);
    P2OUT&=~(BIT5);
}
void Avancer_Robot(){
    P2OUT&=~(BIT1);
    P2OUT|=(BIT5);
}

void Gauche_Robot(){
    P2OUT|=(BIT1|BIT5);
}

void Droite_Robot(){
    P2OUT&=~(BIT1|BIT5);
}

void Arret_Moteur(){
    // TOR les 2
    //P2OUT&=~(BIT2|BIT4); Tout ou rien

    // PWM gauche
    P2OUT&=~(BIT4); // Tout ou rien sur le BIT 4
    TA1CCR1 = 0; // Vitesse de la roue droite 锟?l'arr闃?

    // PWM droit
    P2OUT&=~(BIT2); // Tout ou rien sur le BIT2
    TA1CCR2 = 0; // Vitesse de la roue gauche 锟?l'arr闃?
}

void Marche_Moteur(){
    // TOR les 2
    //P2OUT|=(BIT2|BIT4);

    // >>> METTRE LES 2 EN PWM CAR LES MOTEURS NE SONT PAS LES M钄扙S <<

    // PWM gauche
    P2OUT|=(BIT4);
    TA1CCR1 = 500; // Vitesse pour la roue gauche : 1990/2000

    //PWM droit
    P2OUT|=(BIT2); // Tout ou rien sur le BIT2
    TA1CCR2 = 490; //Vitesse de la roue droite : 1970/2000
}

void InitUART(void)
{
    P1SEL |= (BIT1 + BIT2);                 // P1.1 = RXD, P1.2=TXD
    P1SEL2 |= (BIT1 + BIT2);                // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 |= UCSSEL_2;                   // SMCLK
    UCA0BR0 = 104;                          // 1MHz, 9600
    UCA0BR1 = 0;                            // 1MHz, 9600
    UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;
    UCA0CTL0 &= ~UC7BIT & ~UCSPB & ~UCMODE1;
    UCA0CTL0 &= ~UCMODE0 & ~UCSYNC;
    UCA0CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**
}

void init_USCI( void )
{
    // Waste Time, waiting Slave SYNC
    __delay_cycles(250);

    // SOFTWARE RESET - mode configuration
    UCB0CTL0 = 0;
    UCB0CTL1 = (0 + UCSWRST*1 );

    // clearing IFg /16.4.9/p447/SLAU144j
    // set by setting UCSWRST just before
    IFG2 &= ~(UCB0TXIFG | UCB0RXIFG);

    // Configuration SPI (voir slau144 p.445)
    // UCCKPH = 0 -> Data changed on leading clock edges and sampled on trailing edges.
    // UCCKPL = 0 -> Clock inactive state is low.
    //   SPI Mode 0 :  UCCKPH * 1 | UCCKPL * 0
    //   SPI Mode 1 :  UCCKPH * 0 | UCCKPL * 0  <--
    //   SPI Mode 2 :  UCCKPH * 1 | UCCKPL * 1
    //   SPI Mode 3 :  UCCKPH * 0 | UCCKPL * 1
    // UCMSB  = 1 -> MSB premier
    // UC7BIT = 0 -> 8 bits, 1 -> 7 bits
    // UCMST  = 0 -> CLK by Master, 1 -> CLK by USCI bit CLK / p441/16.3.6
    // UCMODE_x  x=0 -> 3-pin SPI,
    //           x=1 -> 4-pin SPI UC0STE active high,
    //           x=2 -> 4-pin SPI UC0STE active low,
    //           x=3 -> i瞔.
    // UCSYNC = 1 -> Mode synchrone (SPI)
    UCB0CTL0 |= ( UCMST | UCMODE_0 | UCSYNC );
    UCB0CTL0 &= ~( UCCKPH | UCCKPL | UCMSB | UC7BIT );
    UCB0CTL1 |= UCSSEL_2;

    UCB0BR0 = 0x0A;     // divide SMCLK by 10
    UCB0BR1 = 0x00;

    // SPI : Fonctions secondaires
    // MISO-1.6 MOSI-1.7 et CLK-1.5
    // Ref. SLAS735G p48,49
    P1SEL  |= ( SCK | DATA_OUT | DATA_IN);
    P1SEL2 |= ( SCK | DATA_OUT | DATA_IN);

    UCB0CTL1 &= ~UCSWRST;                                // activation USCI
}


void RXdata(unsigned char *c)
{
    while (!(IFG2&UCA0RXIFG));              // buffer Rx USCI_A0 plein ?
    *c = UCA0RXBUF;
}

void TXdata( unsigned char c )
{
    while (!(IFG2&UCA0TXIFG));              // buffer Tx USCI_A0 vide ?
    UCA0TXBUF = c;
}

void Send_STR_UART(const char *msg)
{
    int i = 0;
    for(i=0 ; msg[i] != 0x00 ; i++)
    {
        TXdata(msg[i]);
    }
}

void Send_char_SPI(unsigned char carac)
{
    while ((UCB0STAT & UCBUSY));   // attend que USCI_SPI soit dispo.
    while(!(IFG2 & UCB0TXIFG)); // p442
    UCB0TXBUF = carac;              // Put character in transmit buffer
    //Send_STR_UART("\r\nfonction SPI");   // slave echo
}

void Rec_char_SPI()
{

    while ((UCB0STAT & UCBUSY));   // attend que USCI_SPI soit dispo.
    while(!(IFG2 & UCB0RXIFG)); // p442
    car = UCB0RXBUF;
    __delay_cycles(25000);

    // Put character in buffer
    /*while (!(IFG2&UCB0TXIFG));              // buffer Tx USCI_B0 vide ?
    UCB0TXBUF = car;*/
    TXdata(car);
    //Send_STR_UART("\r\nrec SPI");   // slave echo
}


void command( unsigned char cmd )
{

    switch(cmd)
    {
        case 'h':
            Send_STR_UART("\r\naffichage de cette aide");
            break;
        case 'g':
            Avancer_Robot();
            Marche_Moteur();
            break;
        case 'b':
            Reculer_Robot();
            Marche_Moteur();
            break;
        case 'l':
            Gauche_Robot();
            Marche_Moteur();
            break;
        case 'r':
            Droite_Robot();
            Marche_Moteur();
            break;
        case 's':
            Arret_Moteur();
            Send_STR_UART("\r\nstop");
            break;
        case 'x': //servomotor tourne ver la gauche
            if(TA0CCR1 > 900)
            {
                TA0CCR1 = TA0CCR1-100;
            }
            break;
        case 'c': //droite
            if(TA0CCR1 < 2100)
            {
                TA0CCR1 = TA0CCR1+100;
            }
            break;
            //tester SPI
        case '0':
            Send_char_SPI(0x30);
            Send_STR_UART("\r\nLED OFF");
            break;
        case '1':
            Send_char_SPI(0x31);
            Send_STR_UART("\r\nLED ON");
            break;
        case '2':
            Rec_char_SPI();
            break;
        case '3':
            Send_char_SPI(0x33);
            __delay_cycles(25000);
            Send_STR_UART("\r\n");
            Rec_char_SPI();
            break;
        case 'u':
            //Send_char_SPI(0x33);
            Send_char_SPI('u');
            __delay_cycles(25000);
            Send_STR_UART("\r\n");
            Rec_char_SPI();
            break;
        default:
            break;
    }

}
void IniPWMServo()
{
        P2DIR |= BIT6;
        P2SEL |= (BIT6);
        P2SEL2 &=~ (BIT6);
        P2SEL &=~ (BIT7);
        P2SEL2 &=~ (BIT7);
        TA0CTL = TASSEL_2 | MC_1 ; // tmclk mode up
        //PWM OUTPUT
        TA0CCTL1 |= OUTMOD_7;
        // Limit PWM
        TA0CCR0 = 20000;
        TA0CCR1 = 1500;
        //TA0CTL |= TAIE;

}


int main(void)
{
    unsigned char c;

    WDTCTL = WDTPW + WDTHOLD; // Stop Watch Dog Timer
    BCSCTL1= CALBC1_1MHZ; //frequence d'horloge 1MHz
    DCOCTL= CALDCO_1MHZ; //

    Init_Robot(); // Demarrage du robot
    InitUART();
    init_USCI();
    IniPWMServo();

    P2SEL |= (BIT2|BIT4);
    P2SEL2 &=~ (BIT2|BIT4);

    TA1CTL = TASSEL_2 | MC_1 | ID_0; // tmclk mode up

    //PWM gauche
    TA1CCTL1 |= OUTMOD_7;

    //PWM droit
    TA1CCTL2 |= OUTMOD_7;

    // Limite PWM
    TA1CCR0 = 2000;

    TA1CTL |= TAIE; //autorisation interruption TAIE

    __enable_interrupt();
    memoTimer = cptTimer;
    Send_STR_UART("Robot Ready !");
    while(1)
    {
        switch(state)
        {
        case 1:
            RXdata(&c);
            TXdata(c);
            command(c);
            //state = 100;
            break;
        default:
            break;
        }
    }

}
#pragma vector=TIMER1_A1_VECTOR // Timer
__interrupt void ma_fnc_timer(void)
{
    cptTimer++;
    TA1CTL &= ~TAIFG; //Remise a zero FLAG
}
