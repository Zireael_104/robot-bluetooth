#include <msp430.h> 


/**
 * main.c
 */

volatile unsigned int cptTimer=0; // Compteur
unsigned int memoTimer =0; // Dur? d'une ?ape

unsigned char state=1;

#define CMDLEN  12               // longueur maximum de la commande utilisateur
#define LF      0x0A
#define CR      0x0D

void Init_Robot(){ // D?arrage du robot
    P2DIR|=BIT1; // Sens de la roue droite
    P2OUT|=BIT1;

    P2DIR|=BIT2; // Fonctionnement de la roue gauche
    P2OUT&=~BIT2;

    P2DIR|=BIT5; // Sens de la roue gauche
    P2OUT|=BIT5;

    P2DIR|=BIT4; // Fonctionnement de la roue droite
    P2OUT&=~BIT4;


    __enable_interrupt();
}

void Reculer_Robot(){
    P2OUT|=(BIT1);
    P2OUT&=~(BIT5);
}
void Avancer_Robot(){
    P2OUT&=~(BIT1);
    P2OUT|=(BIT5);
}

void Gauche_Robot(){
    P2OUT|=(BIT1|BIT5);
}

void Droite_Robot(){
    P2OUT&=~(BIT1|BIT5);
}

void Arret_Moteur(){
    // TOR les 2
    //P2OUT&=~(BIT2|BIT4); Tout ou rien

    // PWM gauche
    P2OUT&=~(BIT4); // Tout ou rien sur le BIT 4
    TA1CCR1 = 0; // Vitesse de la roue droite ? l'arr?

    // PWM droit
    P2OUT&=~(BIT2); // Tout ou rien sur le BIT2
    TA1CCR2 = 0; // Vitesse de la roue gauche ? l'arr?
}

void Marche_Moteur(){
    // TOR les 2
    //P2OUT|=(BIT2|BIT4);

    // >>> METTRE LES 2 EN PWM CAR LES MOTEURS NE SONT PAS LES M?ES <<

    // PWM gauche
    P2OUT|=(BIT4);
    TA1CCR1 = 1990; // Vitesse pour la roue gauche : 1990/2000

    //PWM droit
    P2OUT|=(BIT2); // Tout ou rien sur le BIT2
    TA1CCR2 = 1970; //Vitesse de la roue droite : 1960/2000
}

void InitUART(void)
{
    P1SEL |= (BIT1 + BIT2);                 // P1.1 = RXD, P1.2=TXD
    P1SEL2 |= (BIT1 + BIT2);                // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 |= UCSSEL_2;                   // SMCLK
    UCA0BR0 = 104;                          // 1MHz, 9600
    UCA0BR1 = 0;                            // 1MHz, 9600
    UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;
    UCA0CTL0 &= ~UC7BIT & ~UCSPB & ~UCMODE1;
    UCA0CTL0 &= ~UCMODE0 & ~UCSYNC;
    UCA0CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**
}


void RXdata(unsigned char *c)
{
    while (!(IFG2&UCA0RXIFG));              // buffer Rx USCI_A0 plein ?
    *c = UCA0RXBUF;
}

void TXdata( unsigned char c )
{
    while (!(IFG2&UCA0TXIFG));              // buffer Tx USCI_A0 vide ?
    UCA0TXBUF = c;
}

void Send_STR_UART(const char *msg)
{
    int i = 0;
    for(i=0 ; msg[i] != 0x00 ; i++)
    {
        TXdata(msg[i]);
    }
}

void command( char *cmd )
{
  if(cmd == 'h')          // aide
  {
    //P1OUT |= BIT6;
    Send_STR_UART("\r\nAide : commandes existantes :\n");
    Send_STR_UART("\r\n'ver' : version\n");
    Send_STR_UART("\r\n'l' : allumage led\n");
    Send_STR_UART("\r\n'e' : extinction led\n");
    Send_STR_UART("\r\n'h' : affichage de cette aide\n");
  }
  else if (cmd == 'g')      // version
  {
    Marche_Moteur();
    Send_STR_UART("\rDebug MSP430 v1.0\n");
  }
  else
  {
    Send_STR_UART("\rMauvaise commande ");
    Send_STR_UART(cmd);
    Send_STR_UART("\rEntrez 'h' pour l'aide\n");
  }
}


int main(void)
{
    unsigned char c;
    char  cmd;      // tableau de caractere lie a la commande user
    int   nb_car;           // compteur nombre carateres saisis

    WDTCTL = WDTPW + WDTHOLD; // Stop Watch Dog Timer
    BCSCTL1= CALBC1_1MHZ; //frequence d?orloge 1MHz
    DCOCTL= CALDCO_1MHZ; //

    Init_Robot(); // D?arrage du robot
    InitUART();

    P2SEL |= (BIT2|BIT4);
    P2SEL2 &=~ (BIT2|BIT4);

    TA1CTL = TASSEL_2 | MC_1 | ID_0; // tmclk mode up

    //PWM gauche
    TA1CCTL1 |= OUTMOD_7;

    //PWM droit
    TA1CCTL2 |= OUTMOD_7;

    // Limite PWM
    TA1CCR0 = 2000;

    TA1CTL |= TAIE; //autorisation interruption TAIE

    __enable_interrupt();
    memoTimer = cptTimer;
    nb_car = 0;
    Send_STR_UART("Robot Ready !");
    while(1)
    {
        RXdata(&c);
        TXdata(c);
        command(c);

        }
//        switch(state)
//        {
//        case 1:
//            if()//20s
//                    {
//                        Marche_Moteur();
//                    }
//            state = 100;
//            break;
//        default:
//            break;
//        }
    }


#pragma vector=TIMER1_A1_VECTOR // Timer
__interrupt void ma_fnc_timer(void)
{
    cptTimer++;
    TA1CTL &= ~TAIFG; //Remise ? z?o FLAG
}
